#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <SPI.h>
#include <nRF24L01.h>
#include "printf.h"
#include <RF24.h>
#define CE_PIN 9
#define CSN_PIN 10

LiquidCrystal_I2C lcd(0x27, 16, 2);

RF24 radio(CE_PIN, CSN_PIN);

//This is the LED node.
//adress LED node: "lednode"
//adress start/end node: "controlnode"

//Protocol:
//"!start" send from controlnode -> starts timer.
//"!stop" send from controlnode -> stops timer and sends results back.
//"!time {payload}" send from lednode -> send time to controlnode as a response to stop.

uint8_t address[][12] = {"LEDNode", "controlNode"};
String dataReceived = "";

void setup() {

  Serial.begin(115200);

  while (!Serial) {
    // some boards need to wait to ensure access to serial over USB
  }
  
  Serial.println("Serial checked.");
  delay(500);

    if (!radio.begin()) {

    Serial.println("startup failed.");
    delay(1000);
    
    Serial.println(F("Radio hardware is not responding!"));
    while (1) {} // hold in infinite loop
  }

  Serial.println("Radio started.");

  //Set to low to prevent power issues over short distances.
  //radio.setPALevel(RF24_PA_LOW);  // RF24_PA_MAX is default but not needed.

  radio.openWritingPipe(address[1]);
//  radio.openReadingPipe(0, address[0]);
//  radio.stopListening();

  Serial.println("Pipes started.");

  Serial.println("Startup complete");
  Serial.println("Please give a command");
}

void loop() {

  if (Serial.available()){
        String inputString = Serial.readString();
        radio.write(&inputString, sizeof(inputString));
        Serial.print("command: ");
        Serial.print(inputString);
        
  }
   
  if (radio.available()) {
    uint8_t bytes = radio.getPayloadSize(); // get the size of the payload
    radio.read(&dataReceived, bytes);       // fetch data from FIFO
    Serial.print("Received data: ");
    Serial.println(dataReceived);           // print the data received

    String command = getValue(dataReceived, ' ', 0);
    String timerValue = getValue(dataReceived, ' ', 1);
    if (command == "!time") {
      Serial.println(timerValue);
      //Do something with the timervalue.
    }
  }
}

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
