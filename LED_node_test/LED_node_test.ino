#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <SPI.h>
#include <nRF24L01.h>
#include "printf.h"
#include <RF24.h>
#define CE_PIN 9
#define CSN_PIN 10

LiquidCrystal_I2C lcd(0x27, 16, 2);

RF24 radio(CE_PIN, CSN_PIN);

//This is the LED node.
//adress LED node: "lednode"
//adress start/end node: "controlnode"

//Protocol:
//"!start" send from controlnode -> starts timer.
//"!stop" send from controlnode -> stops timer and sends results back.
//"!time {payload}" send from lednode -> send time to controlnode as a response to stop.

uint8_t address[][12] = {"LEDNode", "controlNode"};
String dataReceived = "";
int currentMillis = 0;
bool timerRunning = false;          //This should always be false unless you are testing the timer.

void setup() {

lcd.begin();
lcd.backlight();
lcd.clear();
lcd.setCursor(0, 0);
lcd.print("Starting...");
delay(1000);

Serial.begin(115200);

while (!Serial) {
    // some boards need to wait to ensure access to serial over USB
    lcd.clear();
    Serial.println("Serial checked.");
    lcd.print("checking serial.");
    delay(500);
}

  if (!radio.begin()) {

    lcd.clear();
    Serial.println("startup failed.");
    lcd.print("startup failed.");
    delay(1000);

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Radio not ");
    lcd.setCursor(0, 1);
    lcd.print("responding.");
    delay(1000);
    
    Serial.println(F("Radio hardware is not responding!"));
    while (1) {} // hold in infinite loop
  }

lcd.clear();
lcd.setCursor(0, 0);
lcd.print("Radio started.");
Serial.println("Radio started.");
delay(1000);

//Set to low to prevent power issues over short distances.
//radio.setPALevel(RF24_PA_LOW);  // RF24_PA_MAX is default but not needed.

//radio.openWritingPipe(address[0]);
radio.openReadingPipe(0, address[1]);
radio.startListening();

lcd.clear();
lcd.print("Pipes started.");
Serial.println("Pipes started.");
delay(1000);

lcd.clear();
lcd.println("Startup complete");
Serial.println("Startup complete");
delay(1000);

Serial.println("Waiting on a command");
}

void loop() {

  uint8_t pipe;
  if (radio.available(&pipe)) {
    uint8_t bytes = radio.getPayloadSize(); // get the size of the payload

    radio.read(&dataReceived, bytes);       // fetch data from FIFO
    Serial.print("Received data: ");
    Serial.print((String) dataReceived);    // print the data received
    Serial.print(" On pipe: ");
    Serial.println(pipe);

    if (dataReceived == "!start") {
      timerRunning = true;
      currentMillis = millis();
      dataReceived = "";
    }

    if (dataReceived == "!stop") {
      if (!timerRunning) {
        Serial.println("stop was called but no timer running");
        
      } else {
        int timeDifference =  millis()- currentMillis;
        String payload = "!time " + timeDifference;

        bool result;
        timerRunning = false;
        currentMillis = 0;
        
        result = radio.write((uint8_t *)&timeDifference, 2);

        if (result) {
        // Announces that it has succeeded.
        Serial.println("data has been received");
        } else {
        // Announces that it has failed.
        Serial.println("Send has failed");

        dataReceived = "";
        }
      }
    }

    if (dataReceived == "!reset") {
      timerRunning = false;
      currentMillis = 0;
      dataReceived = "";
    }
  }

  if (timerRunning) {
    lcd.clear();
    long temp = millis()- currentMillis;
    lcd.print(temp);
    delay(100);
  } else {
    delay(1000);
  }
}
